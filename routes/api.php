<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/company', [App\Http\Controllers\Api\Company\PostCreateCompanyController::class, '__invoke']);
Route::post('/enablecompany', [App\Http\Controllers\Api\Company\PostEnableCompanyController::class, '__invoke']);
Route::post('/listcompanies', [App\Http\Controllers\Api\Company\PostListCompanyController::class, '__invoke']);
Route::post('/listcompaniesbystatus', [App\Http\Controllers\Api\Company\PostListByStatusCompanyController::class, '__invoke']);
