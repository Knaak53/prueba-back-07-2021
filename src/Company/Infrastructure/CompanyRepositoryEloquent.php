<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Illuminate\Database\Eloquent\Collection;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::Create([
            'id'     => $company->id(),
            'name'   => $company->name(),
            'email'   => $company->email(),
            'address'   => $company->address(),
            'status' => $company->status(),
        ]);
    }

    /**
     * @param int $id
     * @return Company
     */

    public function getCompanyById(CompanyId $id): ?Company
    {
        /**
         * @var ModelsCompany $eloCompany
         */
        $modelCompany = ModelsCompany::find($id);

        if($modelCompany == null){
            return null;
        }

        return $modelCompany->company;
    }

    public function enable(CompanyId $id): ?Company
    {
        $modelCompany = ModelsCompany::find($id);

        if(!$modelCompany){
            return null;
        };

        if($modelCompany->company->status() == CompanyStatus::disabled()){
            $modelCompany->status = new CompanyStatus(CompanyStatus::ENABLED);
            $modelCompany->save();
        }

        return $modelCompany->company;
    }

    public function getCompanyAll(): Collection
    {
        return ModelsCompany::all();
    }

    public function getCompaniesByStatus(CompanyStatus $status): Collection
    {
        return ModelsCompany::where('status', $status->code())->get();
    }

}
