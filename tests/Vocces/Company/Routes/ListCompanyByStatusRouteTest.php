<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;

class ListCompanyByStatusRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function postCreateNewCompanyRoute()
    {
        /**
         * Preparing
         */

        $statusArray = [
            'active' => 1,
            'inactive' => 2,
        ];

        /**
         * Actions
         */
        $responseStatusActive = $this->json('POST', '/api/listcompaniesbystatus', [
            'status' => $statusArray['active'],
        ]);
        $responseStatusInactive = $this->json('POST', '/api/listcompaniesbystatus', [
            'status' => $statusArray['inactive'],
        ]);

        /**
         * Asserts
         */
        $responseStatusActive->assertStatus(201);
        $responseStatusInactive->assertStatus(201);
    }
}
