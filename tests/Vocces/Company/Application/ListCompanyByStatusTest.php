<?php

namespace Test\Vocces\Company\Application;


use Tests\TestCase;
use Tests\Vocces\Company\Infrastructure\CompanyRepositoryFake;
use Vocces\Company\Application\CompanyListerByStatus;

final class ListCompanyByStatusTest extends TestCase
{
    /**
     * @group application
     * @group company
     * @test
     *
     */
    public function listCompaniesByStatus()
    {
        /**
         * Preparing
         */

        $statusArray = [
            'active' => 1,
            'inactive' => 2,
        ];

        /**
         * Actions
         */


        $lister = new CompanyListerByStatus(new CompanyRepositoryFake());
        $companiesActives = $lister->handle($statusArray['active']);
        $companiesInactives = $lister->handle($statusArray['inactive']);

        /**
         * Assert
         */
        $this->assertNotEmpty($companiesActives);
        $this->assertNotEmpty($companiesInactives);
    }
}
