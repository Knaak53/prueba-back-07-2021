<?php

namespace Test\Vocces\Company\Application;

use \App\Models\Company as ModelCompany;
use Tests\TestCase;
use Vocces\Company\Application\CompanyEnabler;
use Tests\Vocces\Company\Infrastructure\CompanyRepositoryFake;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

final class EnableCompanyTest extends TestCase
{
    /**
     * @group application
     * @group company
     * @test
     *
     */
    public function enableCompany()
    {
        /**
         * Preparing
         */
        /**
         * Preparing
         */
        $modelCompany = ModelCompany::where('status', 2)->first();

        /**
         * Actions
         */


        $enabler = new CompanyEnabler(new CompanyRepositoryFake());
        $enabledCompany = $enabler->handle(new CompanyId($modelCompany->id));

        /**
         * Assert
         */
        $this->assertEquals( CompanyStatus::ENABLED, $enabledCompany->status()->code());
        $this->assertInstanceOf(Company::class, $enabledCompany);
    }
}
