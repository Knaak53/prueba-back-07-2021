<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;

class ListCompanyRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function postCreateNewCompanyRoute()
    {
        /**
         * Preparing
         */

        /**
         * Actions
         */
        $response = $this->json('POST', '/api/listcompanies', []);

        /**
         * Asserts
         */
        $response->assertStatus(201);
    }
}
