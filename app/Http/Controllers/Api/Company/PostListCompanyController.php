<?php

namespace App\Http\Controllers\Api\Company;


use App\Http\Requests\Company\ListCompanyRequest;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyLister;

class PostListCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\CreateCompanyRequest $request
     */
    public function __invoke(ListCompanyRequest $request, CompanyLister $service)
    {
        try {
            $companies = $service->handle();
            return response($companies, 201);
        } catch (\Throwable $error) {
            throw $error;
        }
    }
}
