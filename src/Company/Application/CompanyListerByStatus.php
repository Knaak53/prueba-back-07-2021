<?php

namespace Vocces\Company\Application;

use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyListerByStatus implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private CompanyRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Enables a company
     *
     */
    public function handle($status)
    {

        $companies = $this->repository->getCompaniesByStatus(new CompanyStatus($status));

        return $companies;
    }
}
