<?php

namespace Vocces\Company\Application;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyEnabler implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private CompanyRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Enables a company
     *
     */
    public function handle($id)
    {
        $company = $this->repository->getCompanyById(new CompanyId($id));

        if($company == null) return null;

        if($company->status() == CompanyStatus::enabled()){
            return $company;
        }

        $updatedCompany = new Company(
            $company->id(),
            $company->name(),
            $company->email(),
            $company->address(),
            CompanyStatus::enabled()
        );

        $this->repository->enable($company->id());

        return $updatedCompany;
    }
}
