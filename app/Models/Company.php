<?php

namespace App\Models;

use App\Casts\CompanyAddressCast;
use App\Casts\CompanyCast;
use App\Casts\CompanyEmailCast;
use App\Casts\CompanyIdCast;
use App\Casts\CompanyNameCast;
use App\Casts\CompanyStatusCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'address',
    ];

    protected $casts = [
      'id' => CompanyIdCast::class,
      'name' => CompanyNameCast::class,
      'email' => CompanyEmailCast::class,
      'address' => CompanyAddressCast::class,
      'status' => CompanyStatusCast::class,
      'company' => CompanyCast::class
    ];
}
