<?php

namespace App\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class CompanyCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, $key, $value, $attributes)
    {
        return new Company(
            new CompanyId($attributes['id']),
            new CompanyName($attributes['name']),
            new CompanyEmail($attributes['email']),
            new CompanyAddress($attributes['address']),
            new CompanyStatus($attributes['status'])
        );
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, $key, $value, $attributes)
    {
        return [
            'id' => $value->id(),
            'name' => $value->name(),
            'email' => $value->email(),
            'address' => $value->address(),
            'status' => $value->status(),
        ];
    }
}
