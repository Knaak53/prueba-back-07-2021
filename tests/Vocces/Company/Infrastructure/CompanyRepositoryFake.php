<?php

namespace Tests\Vocces\Company\Infrastructure;

use Illuminate\Database\Eloquent\Collection;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Infrastructure\CompanyRepositoryEloquent;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodCreate = false;
    public bool $callMethodEnable = false;
    public bool $callMethodGetCompanyById = false;
    public CompanyRepositoryEloquent $repository;


    public function __construct()
    {
        $this->repository = new CompanyRepositoryEloquent();
    }

    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
        $this->repository->create($company);
    }

    /**
     * @inheritdoc
     */
    public function enable(CompanyId $id): ?Company
    {
        $this->callMethodEnable = true;
        return $this->repository->enable($id);
    }

    /**
     * @inheritdoc
     */
    public function getCompanyById(CompanyId $id): ?Company
    {
        $this->callMethodGetCompanyById = true;
        return $this->repository->getCompanyById($id);
    }

    public function getCompaniesByStatus(CompanyStatus $status): Collection
    {
        return $this->repository->getCompaniesByStatus($status);
    }

    public function getCompanyAll(): Collection
    {
        return $this->repository->getCompanyAll();
    }
}
