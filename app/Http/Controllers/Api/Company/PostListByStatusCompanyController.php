<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Requests\Company\ListCompanyRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyListerByStatus;

class PostListByStatusCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\CreateCompanyRequest $request
     */
    public function __invoke(ListCompanyRequest $request, CompanyListerByStatus $service)
    {
        DB::beginTransaction();
        try {
            $companies = $service->handle($request->status);
            return response($companies, 201);
        } catch (\Throwable $error) {
            throw $error;
        }
    }
}
