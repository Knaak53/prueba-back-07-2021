<?php

namespace Test\Vocces\Company\Application;

use Tests\TestCase;
use Tests\Vocces\Company\Infrastructure\CompanyRepositoryFake;
use Vocces\Company\Application\CompanyLister;

final class ListCompanyTest extends TestCase
{
    /**
     * @group application
     * @group company
     * @test
     *
     */
    public function listCompanies()
    {
        /**
         * Preparing
         */

        /**
         * Actions
         */


        $lister = new CompanyLister(new CompanyRepositoryFake());
        $companies = $lister->handle();

        /**
         * Assert
         */
        $this->assertNotEmpty($companies);
    }
}
