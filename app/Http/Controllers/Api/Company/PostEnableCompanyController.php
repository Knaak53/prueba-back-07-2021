<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Requests\Company\EnableCompanyRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyEnabler;

class PostEnableCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\CreateCompanyRequest $request
     */
    public function __invoke(EnableCompanyRequest $request, CompanyEnabler $service)
    {
        DB::beginTransaction();
        try {
            $company = $service->handle($request->id);
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
