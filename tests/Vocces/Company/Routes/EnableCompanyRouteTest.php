<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;
use App\Models\Company as ModelCompany;

class EnableCompanyRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function postCreateNewCompanyRoute()
    {
        /**
         * Preparing
         */
        $modelCompany = ModelCompany::where('status', 2)->first();

        $companyArray = $modelCompany->company->toArray();
        $companyArray['status'] = 'active';
        /**
         * Actions
         */
        $response = $this->json('POST', '/api/enablecompany', [
            'id' => $modelCompany->id->get(),
        ]);

        /**
         * Asserts
         */
        $response->assertStatus(201)
            ->assertJsonFragment($companyArray);
    }
}
