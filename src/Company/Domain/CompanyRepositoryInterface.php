<?php

namespace Vocces\Company\Domain;

use Illuminate\Database\Eloquent\Collection;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

interface CompanyRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return void
     */
    public function create(Company $company): void;

    public function enable(CompanyId $id): ?Company;

    public function getCompanyById(CompanyId $id): ?Company;

    public function getCompaniesByStatus(CompanyStatus $status): Collection;

    public function getCompanyAll(): Collection;



}
